<?php
class HeadwayFeaturedPostsBlock extends HeadwayBlockAPI {

	public $id = 'featured-posts';
	
	public $name = 'Featured Posts Carousel';
	
	public $options_class = 'HeadwayFeaturedPostsBlockOptions';
	
// Link na archiv post type
 public  function get_archive_link( $post_type ) {
    global $wp_post_types;
    $archive_link = false;
    if (isset($wp_post_types[$post_type])) {
      $wp_post_type = $wp_post_types[$post_type];
      if ($wp_post_type->publicly_queryable)
        if ($wp_post_type->has_archive && $wp_post_type->has_archive!==true)
          $slug = $wp_post_type->has_archive;
        else if (isset($wp_post_type->rewrite['slug']))
          $slug = $wp_post_type->rewrite['slug'];
        else
          $slug = $post_type;
      		$archive_link = get_option( 'siteurl' ) . "/{$slug}/";
    }
    return apply_filters( 'archive_link', $archive_link, $post_type );
  }
// END Link na archiv post type
	function init() {

	}

	function enqueue_action($block_id) {
		/* CSS */
		wp_enqueue_style('headway-featured-posts', plugins_url(basename(dirname(__FILE__))) . '/css/featured-posts.css');
		/* JS */
		wp_enqueue_script('headway-carousfredsel', plugins_url(basename(dirname(__FILE__))) . '/js/min.caroufredsel.js', array('jquery'));
	//	wp_enqueue_script('headway-flipper-posts', plugins_url(basename(dirname(__FILE__))) . '/js/flipper.js', array('jquery'));
		wp_enqueue_script('headway-easing-posts', plugins_url(basename(dirname(__FILE__))) . '/js/script.easing.js', array('jquery'));

		/* Variables */
		// wp_localize_script('headway-featured-posts', 'HeadwayFeaturedPosts', array(
		// 	'ajaxURL' => admin_url('admin-ajax.php')
		// ));
	}

    function dynamic_js($block_id, $block = false) {
	
		if ( !$block )
		$block = HeadwayBlocksData::get_block($block_id);
		//$easing_animation = parent::get_setting($block, 'easing-animation', 'elastic');

		$scroll_speed = parent::get_setting($block,'scroll_speed', '800');
		//	$transition_animation = parent::get_setting($block,'transition-animation', 'left');
		
		$show_pager = parent::get_setting($block, 'show-pager', '.pager');
		
		$js="

		!function ($) {
		$(window).load(function(){
	
		$('.flipper').each(function(){
			
	    	var flipper = $(this)
			,	scrollSpeed
			, 	easing
			, 	direction
			,	responsive
			, 	shownmin
			, 	shownmax
			, 	shownmin = flipper.data('shown-min') || 1
			, 	shownmax = flipper.data('shown-max') || 1
			,	scrollSpeed = flipper.data('scroll-speed') || 700
			,	easing = flipper.data('easing') || 'linear'
			,	fx = flipper.data('fx') || 'scroll'
			,	direction = flipper.data('direction') || left
			,	responsive = flipper.data('responsive') || false
			,	width = flipper.data('width') || '100%'
		
	    	flipper.carouFredSel({

						// height: '100%'
			 			height: 'variable'
				 		,direction: direction    // The direction to scroll the carousel. Possible values: 'right', 'left', 'up' or 'down'.
	    		 		, responsive: responsive
	    				
	    				//, responsive: true
	    				//,circular: 	true

				, onCreate: function(){
					
				}

			//	,pagination: '.pager'
				,pagination: '".$show_pager."'

				, items       : {
					width				: width,
					// width			: 250,
					// width			: '100%',
					//	height			: '100%',

				height				: 'variable',
			        visible     : {
			            min         : shownmin
			            , max         : shownmax
			        }
			    }
				
			    , swipe       : {
			        	onTouch     : true
			    }
			    , scroll: {
				    	items: '+1',
				    	easing          : easing
			            ,duration        : scrollSpeed
				        ,fx              : fx
						,pauseOnHover    : true
			    }
				
				,auto     : {
				    play            : true
		        	,duration        :  scroll.duration
					,timeoutDuration :  2 * scroll.duration
				// ,timeoutDuration :  15000 //5 * auto.duration
				}
				
		        , prev    : {
			        button  : function() {
			           return flipper.parents('.flipper-wrap').prev('.flipper-heading').find('.flipper-prev');
			        }
		    	}
			    , next    : {
		       		button  : function() {
			           return flipper.parents('.flipper-wrap').prev('.flipper-heading').find('.flipper-next');
			        }
			    }

		    }).addClass('flipper-loaded').animate({'opacity': 1},800);
		
		//	$.plCommon.plVerticalCenter('.flipper-info', '.pl-center', -20)
		
	    });
		
	})
}(window.jQuery);

		";

		return $js;
	}

	function dynamic_css($block_id) {

	$caption_margin= parent::get_setting($block_id, 'caption-margin', 70) . '%';


	return '

	.flipper-item .item-content {
		top: '.$caption_margin.';
	}';

	}

	/** 
	 * Anything in here will be displayed when the block is being displayed.
	 **/
	function content($block) {
	// echo	$post_type = parent::get_setting($block, 'post-type', 'post');
	// 
		$easing_animation = parent::get_setting($block, 'easing-animation', 'linear');
		$post_type = parent::get_setting($block, 'post-type', false);
		$html_tag = parent::get_setting($block, 'title-html-tag', 'h3');
		
		$fx = parent::get_setting($block,'fx', 'scroll');
		$scroll_speed = parent::get_setting($block,'scroll-speed', '800');

		$linked = parent::get_setting($block,'title-link', true);
		$title = parent::get_setting($block, 'title');

		$shown_min = parent::get_setting($block, 'shown-min', 1);		
		$shown_max = parent::get_setting($block, 'shown-max', 4);

		$hide_link_to_post = parent::get_setting($block,'hide-link-to-post', false);

/**
//	$approx_featured_width = (HeadwayBlocksData::get_block_width($block) / $columns);
pozor hadze tuto chybu Warning: Division by zero in
**/

/**
Advanced Excerpt
*/
		$my_excerpt_limit = parent::get_setting($block, 'my-excerpt-limit' , '10');
		$length_type = parent::get_setting($block, 'length-type', 'words'); // words, characters
		$no_custom = parent::get_setting($block, 'no-custom'); // 1, 0  (1 = excerpt  /  0 = custom excerpt will be used)
		$no_shortcode = parent::get_setting($block, 'no-shortcode', 'false'); // 1, 0  (1 = shortcode remove)  
		$finish = parent::get_setting($block, 'finish' ,'exact'); //exact, word, sentence
		
		$read_more = parent::get_setting($block, 'read-more'); //
		
		$add_link = parent::get_setting($block, 'add-link', false); //
		$exclude_tags = parent::get_setting($block, 'exclude-tags'); //
		$allowed_tags = parent::get_setting($block, 'allowed-tags'); //
		$ellipsis = parent::get_setting($block, 'ellipsis'); //
/**

**/
		$direction = parent::get_setting($block,'direction', 'left');

		$show_navigation = parent::get_setting($block, 'show-navigation');
		$prev_nav = parent::get_setting($block, 'prev-nav', 'ss-navigateleft');
		$next_nav = parent::get_setting($block, 'next-nav', 'ss-navigateright');
		
		/* Element Visibility */
		$show_images = parent::get_setting($block, 'show-images', true);
		$width = parent::get_setting($block, 'image-width', '260');

		$show_item_title = parent::get_setting($block, 'show-item-title', false);
		$show_item_title_before = parent::get_setting($block, 'show-item-title-before', true);

	$show_item_excerpt_content = parent::get_setting($block, 'show-item-excerpt-content');
	$item_excerpt_content = parent::get_setting($block, 'item-excerpt-content', 'excerpt-option');
		if 	($direction  == 'up' || $direction == 'down'){
				$responsive = 'false';
				$width = '100%';
		} else {
				$responsive = 'true';
				//$width = '240';
		}

			// $show_titles = parent::get_setting($block, 'show-titles', true);

			/* Content */
				$show_continue = parent::get_setting($block, 'show-continue', false);
				$content_to_show = parent::get_setting($block, 'content-to-show', 'excerpt');

		/* Images */
				$crop_images_vertically = parent::get_setting($block, 'crop-vertically', false);

		/* Setup Query */
			$query_args = array();

			/* Pagination */
				$paged_var = get_query_var('paged') ? get_query_var('paged') : get_query_var('page');

				if ( (parent::get_setting($block, 'paginate', true) || parent::get_setting($block, 'infinite-scroll', true)) && (headway_get('featured-posts-page') || $paged_var) )
					$query_args['paged'] = headway_get('featured-posts-page') ? headway_get('featured-posts-page') : $paged_var;

			/* Categories */
				if ( parent::get_setting($block, 'categories-mode', 'include') == 'include' ) 
					$query_args['category__in'] = parent::get_setting($block, 'categories', array());

				if ( parent::get_setting($block, 'categories-mode', 'include') == 'exclude' ) 
					$query_args['category__not_in'] = parent::get_setting($block, 'categories', array());	

			$query_args['post_type'] = parent::get_setting($block, 'post-type', false);

			/* FeaturedPosts limit */
				$query_args['posts_per_page'] = parent::get_setting($block, 'shown-total', 10);

			/* Author Filter */
				if ( is_array(parent::get_setting($block, 'author')) )
					$query_args['author'] = trim(implode(',', parent::get_setting($block, 'author')), ', ');

			/* Order */
				$query_args['orderby'] = parent::get_setting($block, 'order-by', 'date');
				$query_args['order'] = parent::get_setting($block, 'order', 'DESC');

			/* Status */
				$query_args['post_status'] = 'publish';

			/* Query! */
				$posts = new WP_Query($query_args);

				global $paged; /* Set paged to the proper number because WordPress pagination SUCKS!  ANGER! */
				$paged = $paged_var;
		/* End Query Setup */

/** 
	
	DEBUG 

**/		
	if (parent::get_setting($block, 'debug') == true) 	{
		
	echo '<div class="debug">
	 <pre>';
		print_r($query_args);
		echo 'width - '. $width .'<br>';

		echo 'easing - '. $easing_animation .'<br>';
		echo 'responsive - '.  $responsive.'<br>';
		echo 'fx - '.  $fx .'<br>';
		echo 'scroll speed - '. $scroll_speed .'<br>';
		echo 'direction - '. $direction .'<br>';
		echo 'shown_max - '. $shown_max .'<br>';
		echo 'shown_min - '. $shown_min .'<br>';

		echo 'Prev Nav - '. $prev_nav .'<br>';
		echo 'Next Nav - '. $next_nav .'<br>';
		echo 'Content - '. $content_to_show .'<br>';
		echo 'Show navigation - '. $show_navigation .'<br>';
		echo 'Show Item Title - '. $show_item_title .'<br>';
		echo 'Show Item Excerpt mode - '. $item_excerpt_content .'<br>';

		echo 'Show Item Title Before - '. $show_item_title_before .'<br>';
		echo '</pre></div>';

	}
		if(!empty($posts)) { the_post( $post ); ?>
				<div class="flipper-heading">
				<?php if($title){ ?>
					<div class="flipper-title">
						<?php
						if($linked){
							printf('<%s><a href="%s">%s</a></%s>', $html_tag, self::get_archive_link($post_type), $title,  $html_tag );
						} else {
							printf('<%s>%s</%s>', $html_tag, $title,  $html_tag );
						}
						?>
					</div>
					<?php } ?>
					<?php if ($show_navigation == 'show-options') {  ?>
					<a class="flipper-prev" href="#"><i class="<?php echo $prev_nav; ?>"></i></a>
			    	<a class="flipper-next" href="#"><i class="<?php echo $next_nav; ?>"></i></a>
			    	<?php  } //end if navigation ?>
				</div>
				<div class="flipper-wrap">

				<?php // <ul class="flipper-items text-align-center flipper" data-scroll-speed="1400" data-easing="easeInOutQuart" data-shown="6"> 

				//data-responsive="<?php echo $responsive; ?>
		<ul class="flipper-items text-align-center flipper" data-responsive="<?php echo $responsive; ?>" data-width="<?php echo $width; ?>" data-scroll-speed="<?php echo $scroll_speed; ?>" data-easing="<?php echo $easing_animation; ?>" data-direction="<?php echo $direction; ?>" data-fx="<?php echo $fx; ?>" data-shown-min="<?php echo $shown_min; ?>"  data-shown-max="<?php echo $shown_max; ?>"> 

		<?php } ?>

			<?php
			if(!empty($posts)):
				while ( $posts->have_posts() ) : $posts->the_post();
				?>
			<li>
				<?php if ($show_images) { ?>
				<div class="flipper-item fix">
					<?php
					if ( has_post_thumbnail() ) {
						echo get_the_post_thumbnail( $post->ID, 'featured-thumb', array('title' => ''));
					} else {
						echo '<img height="400" width="600" src="'.plugins_url(basename(dirname(__FILE__))).'/asset/fake-thumb.png" alt="no image added yet" />';
					}
					?>

				<div class="item-content" style="max-width: 1000px;">
					<div class="item-content-pad">
 						<h2><a href="http://demo.cactusthemes.com/ezcarousel/?p=70" title="Accusantium Doloremque">Accusantium</a></h2>
						<div class="item-excerpt"><p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium. Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium.</p>
						</div>
						<a class="item-link" href="http://demo.cactusthemes.com/ezcarousel/?p=70"><i class="icon-anchor"></i> DETAILS</a>
					</div>
				</div>
				
					<div class="flipper-info-bg"></div>
					<?php
					if(!$hide_link_to_post){
						?>
						<a class="flipper-info pl-center-inside" href="<?php echo get_permalink();?>">
							<div class="pl-center">
							<?php
								$featured_image = wp_get_attachment_image_src( get_post_thumbnail_id(), 'featured' );
								printf('<div class="info-text">%s</div>', __("Zobraziť", 'headway'));
							?>
							</div>
						</a>
						<?php } else { ?>
						<div class="pl-center">
						<?php
							$featured_image = wp_get_attachment_image_src( get_post_thumbnail_id(), 'featured' );
							//printf('<div class="info-text">%s</div>', __("View", 'pagelines'));
						?>
						</div>
					<?php }
					?>
				</div><!--work-item-->
				<?php }  // end if images ?>
				<div class="flipper-meta">
				<?php if (($show_item_title) and ($show_item_title_before)) { ?>
					<h4 class="flipper-post-title"><?php the_title(); ?></h4>				
 				<?php } ?>

 				<?php if ( $show_item_excerpt_content == 'show-option') {
						if($item_excerpt_content ==	'excerpt-option'){
							the_excerpt(); 
						} elseif ($item_excerpt_content ==	'content-option'){
							the_content( );
						} elseif ($item_excerpt_content ==	'my-excerpt-option'){

							if(function_exists(the_advanced_excerpt)){
								// TODO

									//If you want to call the filter with different options, you can use `the_advanced_excerpt()` template tag provided by this plugin. This tag accepts [query-string-style parameters](http://codex.wordpress.org/Template_Tags/How_to_Pass_Tag_Parameters#Tags_with_query-string-style_parameters) (theme developers will be familiar with this notation).

									// The following parameters can be set:

									// * `length`, an integer that determines the length of the excerpt
									
									// * `length_type`, enumeration, if set to `words` the excerpt length will be in words; if set to `characters` the excerpt length will be in characters
									
									// * `no_custom`, if set to `1`, an excerpt will be generated even if the post has a custom excerpt; if set to `0`, the custom excerpt will be used
									
									// * `no_shortcode`, if set to `1`, shortcodes are removed from the excerpt; if set to `0`, shortcodes will be parsed
									
									// * `finish`, enumeration, if set to `exact` the excerpt will be the exact lenth as defined by the "Excerpt Length" option. If set to `word` the last word in the excerpt will be completed. If set to `sentence` the last sentence in the excerpt will be completed.
									
									// * `ellipsis`, the string that will substitute the omitted part of the post; if you want to use HTML entities in the string, use `%26` instead of the `&` prefix to avoid breaking the query
									
									// * `read_more`, the text used in the read-more link
									
									// * `add_link`, if set to `1`, the read-more link will be appended; if `0`, no link will be added
									
									// * `allowed_tags`, a comma-separated list of HTML tags that are allowed in the excerpt. Entering `_all` will preserve all tags.
									
									// * `exclude_tags`, a comma-separated list of HTML tags that must be removed from the excerpt. Using this setting in combination with `allowed_tags` makes no sense
								
								// the_advanced_excerpt('length=30&length_type=words&no_custom=1&ellipsis=%26hellip;&exclude_tags=img,p,strong');

								$params = array( 
									'length' => $my_excerpt_limit,
									'length_type' => $length_type,
									'finish' => $finish,
									'no_custom' => $no_custom,
									'ellipsis'	=> $ellipsis,
									'exclude_tags'	=> $exclude_tags,
									'allowed_tags'	=> $allowed_tags,
									'read_more'	=> $read_more,
									'add_link'	=> $add_link,
									);

									 // the_advanced_excerpt('length=30&length_type=characters&no_custom=1&ellipsis=%26hellip;&exclude_tags=img,p,strong');

								 the_advanced_excerpt($params); 
							}
						}
					}
			
					?>
				<?php if (($show_item_title) and (!$show_item_title_before)) { ?>
					<p class="flipper-post-title"><?php the_title(); ?></p>				
 				<?php } ?>
								
				<?php if (parent::get_setting($block, 'show-edit-link')) 	{ ?>
					<?php edit_post_link(); ?>
				<?php } ?>
				</div>
			</li>
			<?php 
		endwhile;
		endif;
			if(!empty($posts))
		 		echo '</ul>
		 	<div class="pager"></div>
			<div class="clear"></div></div>';
}

	/**
	 * Register elements to be edited by the Headway Design Editor
	 **/

	function setup_elements() {

		//	'properties' => array('fonts', 'background', 'borders', 'padding', 'rounded-corners', 'box-shadow', 'text-shadow'),
		// 'parent' => 'flipper-prev-next',


		$this->register_block_element(array(
			'id' => 'featured-title',
			'name' => 'Featured Posts Title',
			'selector' => '.featured-posts-featured .entry-title a',
			'properties' => array('fonts', 'background', 'borders', 'padding', 'rounded-corners', 'box-shadow', 'text-shadow'),
			'states' => array(
				'Hover' => '.featured-posts-featured .entry-title a:hover', 
			)
		));

		$this->register_block_element(array(
			'id' => 'flipper-meta',
			'name' => 'Meta',
			'selector' => '.flipper-meta',
			'states' => array(
				'Hover' => '.flipper-meta:hover', 
			)
		));
		
		$this->register_block_element(array(
			'id' => 'flipper-post-title',
			'name' => 'Title',
			'selector' => 'h4.flipper-post-title',
			'states' => array(
				'Hover' => 'h4 .flipper-post-title:hover', 
			)
		));
	
		$this->register_block_element(array(
			'id' => 'flipper-item',
			'name' => 'Item',
			'selector' => '.flipper-item,',
			'states' => array(
				'Hover' => '.flipper-item:hover', 
			)
		));

		$this->register_block_element(array(
			'id' => 'flipper-item-image',
			'name' => 'Item Image',
			'selector' => '.flipper-item img',
			'states' => array(
				'Hover' => '.flipper-item img:hover', 
			)
		));

		$this->register_block_element(array(
			'id' => 'flipper-prev-next',
			'name' => 'Prev / Next',
			'selector' => '.flipper-next, .flipper-prev',
			'states' => array(
				'Hover' => '.flipper-nex:hover, .flipper-prev:hover', 
			)
		));

		$this->register_block_element(array(
			'id' => 'flipper-prev',
			'name' => 'Prev',
			'parent' => 'flipper-prev-next',
			'selector' => '.flipper-prev',
			'states' => array(
				'Hover' => '.flipper-prev:hover', 
			)
		));

		$this->register_block_element(array(
			'id' => 'item-content',
			'name' => 'Content - caption',
			//'parent' => '', // parent id
			'selector' => '.flipper-item .item-content',
			// 'description' => 'description',
		//	'inspectable' => false, // napr :last, :first-of-type...
		//	'properties' => array('fonts', 'background', 'borders', 'padding', 'rounded-corners', 'box-shadow', 'text-shadow'),
			'states' => array(
				'Hover' => '',
				'Clicked' => '',
				'Selected' => '',
				'Focus' => '', // Forms
				'Active' => '' // Buttons
			)
		));

		$this->register_block_element(array(
			'id' => 'item-content-pad',
			'name' => 'Content pad',
			'parent' => 'item-content', // parent id
			'selector' => '.item-content-pad',
			// 'description' => 'description',
		//	'inspectable' => false, // napr :last, :first-of-type...
		//	'properties' => array('fonts', 'background', 'borders', 'padding', 'rounded-corners', 'box-shadow', 'text-shadow'),
		));

		$this->register_block_element(array(
			'id' => 'flipper-item-item-content-h2',
			'name' => 'Head',
			'parent' => 'item-content', // parent id
			'selector' => '.flipper-item .item-content h2 a',
			'description' => 'description',
		//	'inspectable' => false, // napr :last, :first-of-type...
		//	'properties' => array('fonts', 'background', 'borders', 'padding', 'rounded-corners', 'box-shadow', 'text-shadow'),
			'states' => array(
				'Hover' => '',
				'Clicked' => '',
				'Selected' => '',
				'Focus' => '', // Forms
				'Active' => '' // Buttons
			)
		));
		$this->register_block_element(array(
			'id' => 'flipper-next',
			'name' => 'Next',
			'selector' => '.flipper-next',
			'parent' => 'flipper-prev-next',
			'states' => array(
				'Hover' => '.flipper-next:hover', 
			)
		));

		$this->register_block_element(array(
			'id' => 'flipper-heading-title',
			'name' => 'Heading Title',
			'selector' => '.flipper-heading .flipper-title',
			'states' => array(
				'Hover' => '.flipper-heading .flipper-title:hover', 
			)
		));
		
		$this->register_block_element(array(
			'id' => 'flipper-heading-title-h',
			'name' => 'Heading Title H',
			'selector' => '.flipper-title h1, .flipper-title h2, .flipper-title h3, .flipper-title h4, .flipper-title h5, .flipper-title h6, .flipper-title span',
			'states' => array(
				'Hover' => '.flipper-title h1:hover, .flipper-title h2:hover, .flipper-title h3:hover, .flipper-title h4:hover, .flipper-title h5:hover, .flipper-title h6 :hover, .flipper-title span:hover', 
			)
		));

		$this->register_block_element(array(
			'id' => 'read-more',
			'name' => 'Read more',
			'selector' => 'div.flipper-meta > a.read-more',
			'states' => array(
				'Hover' => ' div.flipper-meta > a.read-more:hover', 
			)
		));
			// 'parent' => 'flipper-prev-next',

		$this->register_block_element(array(
			'id' => 'pager',
			'name' => 'Pager',
			'selector' => '.flipper-wrap > div.pager a',
			'states' => array(
				'Hover' => '.flipper-wrap > div.pager a:hover', 
			)
		));

		$this->register_block_element(array(
			'id' => 'pager-selected',
			'name' => 'Pager Selected',
			'parent' => 'pager',
			'selector' => '.flipper-wrap > div.pager a.selected',
			'states' => array(
				'Hover' => '.flipper-wrap > div.pager a.selected:hover', 
			)
		));
	}
	
}