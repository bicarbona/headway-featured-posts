<?php
/*
Plugin Name: Headway Featured Posts
Plugin URI: https://bitbucket.org/bicarbona/headway-featured-posts
Description: Based on CarouFredsel
Version: 1.9
Author: Etienne
Author URI: http://etienne.sk
License: GNU GPL v2
* Bitbucket Plugin URI: https://bitbucket.org/bicarbona/headway-featured-posts
* Bitbucket Branch: master
*/

if( !in_array( 'xfeatured', get_intermediate_image_sizes() ) ){
	add_image_size( 'xfeatured', 900, 600, true );  // crop
}

/**
 * Everything is ran at the after_setup_theme action to insure that all of Headway's classes and functions are loaded.
 **/
add_action('after_setup_theme', 'featured_posts_block_register');
function featured_posts_block_register() {

	if ( !class_exists('Headway') )
		return;
	
	require_once 'block.php';
	require_once 'block-options.php';

	//require_once 'design-editor-settings.php';

	return headway_register_block('HeadwayFeaturedPostsBlock', plugins_url(false, __FILE__));

}

/**
 * Prevent 404ing from breaking Infinite Scrolling
 **/
add_action('status_header', 'featured_posts_block_prevent_404');
function featured_posts_block_prevent_404($status) {

	if ( strpos($status, '404') && get_query_var('paged') && headway_get('pb') )
		return 'HTTP/1.1 200 OK';

	return $status;
}

/** 
 * Prevent WordPress redirect from messing up featured board pagination
 */
add_filter('redirect_canonical', 'featured_posts_block_redirect');
function featured_posts_block_redirect($redirect_url) {

	if ( headway_get('pb') )
		return false;

	return $redirect_url;

}