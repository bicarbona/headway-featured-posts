## HELP

### ACF - polia

+ link_to_another_post
+ slider_icon_class
+ VE - nezobrazuje moznost pre ikonu treba zobrazit vsetky elements az potom je viditelny


# FIX

+ check show min post
+ pagination - pri viacnasobnom pouziti!
+ edit link
+ v options opravit zobrazovanie excerpt aj content

### Other fix

+ zjednotit nazov slidera vsade kde ho pouzivam

-

# BUG

-

# ADD

+ LazyLoad
+ link to post aj inym sposobom ako info....
+ pri obrazkoch moznost vlastej nie len definovanej velkosti / vypnutie velkosti
+ ACF - polia
	+ link_to_another_post
	+ slider_icon_class
	(slider icon spravit moznost select boxu / vlastne ikony
		ideal ako pri reduxe ze parsroval priamo css ko
	

+ ACF + CPT - priamo v plugine
+ WP LESS  - pridat a osetrit moznostou css a chybovou hlaskou ak nieje nainstalovane 
+ advaced excerpt - tiez chybova hlaska ... podobne ako u LESS


### COMUNITY

+ zistit ci sa da zobrazit pseudo element napr before vo VE