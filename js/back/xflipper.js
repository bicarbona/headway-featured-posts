!function ($) {
	$(window).load(function(){
	
		$('.flipper').each(function(){
			
	    	var flipper = $(this)
			,	scrollSpeed
			, 	easing
			, 	shown = flipper.data('shown') || 1
			,	scrollSpeed = flipper.data('scroll-speed') || 400
		//	,	easing = flipper.data('easing') || 'elastic'
			//,	easing = flipper.data('easing') || 'easeInOutCubic'
			,	easing = flipper.data('easing') || 'easeInBack'
			
	    	flipper.carouFredSel({
	    		circular: 	true,
	    		// responsive: true // nefunguje pri scroll up / down

				 height: "variable"

				, onCreate: function(){
					
				}
  				,  direction: "up"    // The direction to scroll the carousel. Possible values: "right", "left", "up" or "down".

				, items					: {
					width				: 240,
					height				: "variable",
			        visible     		: {
			            min				: 2
			            , max			: shown
			        }
			    }
				
			    , swipe					: {
			        onTouch				: true
			    }

			    , scroll				: {
			    	easing				: easing
		            ,duration			: scrollSpeed
			        ,fx					: "scroll"
					,pauseOnHover		: true
			    }
				
				,auto     : {
					play				: true
					,timeoutDuration	:  15000 //5 * auto.duration
		        	,duration				:  scroll.duration
				}

		        , prev    : {
			        button  : function() {
			           return flipper.parents('.flipper-wrap').prev(".flipper-heading").find('.flipper-prev');
			        }
		    	}

			    , next    : {
		       		button  : function() {
			           return flipper.parents('.flipper-wrap').prev(".flipper-heading").find('.flipper-next');
			        }
			    }

			    , auto    : {
			    	play: true
			    }
				
		    }).addClass('flipper-loaded').animate({'opacity': 1},1300);
		
		//	$.plCommon.plVerticalCenter('.flipper-info', '.pl-center', -20)
		
	    });
		
	})
}(window.jQuery);