!function ($) {
	$(window).load(function(){
	
		$('.flipper').each(function(){
			
	    	var flipper = $(this)
			,	scrollSpeed
			, 	easing
			, 	direction
			,	responsive
			, 	shown = flipper.data('shown') || 1
			,	scrollSpeed = flipper.data('scroll-speed') || 700
			,	easing = flipper.data('easing') || 'linear'
			,	fx = flipper.data('fx') || 'scroll'
			,	direction = flipper.data('direction') || left
			,	responsive = flipper.data('responsive') || false
			,	width = flipper.data('width') || 240

		
	    	flipper.carouFredSel({

			// height: '100%'
			 height: 'variable'

				// height: "variable"
				 ,direction: direction    // The direction to scroll the carousel. Possible values: 'right', 'left', 'up' or 'down'.

	    		// , responsive: responsive
	    		, responsive: true

	    		//,circular: 	true

				, onCreate: function(){
					
				}

				, items       : {
					// width				: 250,
					// width				: '100%',
					height				: '100%',

					//height				: 'variable',
			        visible     : {
			            min         : 2
			            , max         : shown
			        }
			    }
				
			    , swipe       : {
			        onTouch     : true
			    }
			    , scroll: {
			    	//items: '10'
			    	easing          : easing
		            ,duration        : scrollSpeed
			        ,fx              : fx
					,pauseOnHover     : true
			    }
				
				,auto     : {
				    play            : true
					,timeoutDuration :  15000 //5 * auto.duration
		        	,duration        :  scroll.duration
				
				}
				
		        , prev    : {
			        button  : function() {
			           return flipper.parents('.flipper-wrap').prev(".flipper-heading").find('.flipper-prev');
			        }
		    	}
			    , next    : {
		       		button  : function() {
			           return flipper.parents('.flipper-wrap').prev(".flipper-heading").find('.flipper-next');
			        }
			    }

				
		    }).addClass('flipper-loaded').animate({'opacity': 1},800);
		
		//	$.plCommon.plVerticalCenter('.flipper-info', '.pl-center', -20)
		
	    });
		
	})
}(window.jQuery);