<?php
/**
 * TODO
 *
 * zistit ci sa da zobrazit pseudo element napr before vo VE
 * pozor koliduje s lazy load
 * lazy
 * http://coolcarousels.frebsite.nl/c/14/
 */

class HeadwayFeaturedPostsBlock extends HeadwayBlockAPI {

	public $id = 'featured-posts';
	
	public $name = 'Featured Posts Carousel';
	
	public $options_class = 'HeadwayFeaturedPostsBlockOptions';
	
	protected $show_content_in_grid = true;

// Link na archiv post type
 public  function get_archive_link( $post_type ) {
    global $wp_post_types;
    $archive_link = false;
    if (isset($wp_post_types[$post_type])) {
      $wp_post_type = $wp_post_types[$post_type];
      if ($wp_post_type->publicly_queryable)
        if ($wp_post_type->has_archive && $wp_post_type->has_archive!==true)
          $slug = $wp_post_type->has_archive;
        else if (isset($wp_post_type->rewrite['slug']))
          $slug = $wp_post_type->rewrite['slug'];
        else
          $slug = $post_type;
      		$archive_link = get_option( 'siteurl' ) . "/{$slug}/";
    }
    return apply_filters( 'archive_link', $archive_link, $post_type );
  }
// END Link na archiv post type
	

	function init() {

	}

	function enqueue_action($block_id, $block) {
		/* CSS */
		wp_enqueue_style('headway-featured-posts', plugins_url(basename(dirname(__FILE__))) . '/css/featured-posts.css');
		/* JS */
		wp_enqueue_script('headway-carousfredsel', plugins_url(basename(dirname(__FILE__))) . '/js/min.caroufredsel.js', array('jquery'));
		//	wp_enqueue_script('headway-flipper-posts', plugins_url(basename(dirname(__FILE__))) . '/js/flipper.js', array('jquery'));
		wp_enqueue_script('headway-easing-posts', plugins_url(basename(dirname(__FILE__))) . '/js/script.easing.js', array('jquery'));

		/* Typer */
		if(parent::get_setting($block, 'typer-js')){

			wp_enqueue_script('hw-typer', plugins_url(basename(dirname(__FILE__))) . '/js/jquery.typer.js', array('jquery'));
		
		}
		

		/* Variables */
		// wp_localize_script('headway-featured-posts', 'HeadwayFeaturedPosts', array(
		// 	'ajaxURL' => admin_url('admin-ajax.php')
		// ));
	}



    function dynamic_js($block_id, $block = false) {
	
		if ( !$block )
		$block = HeadwayBlocksData::get_block($block_id);
		//$easing_animation = parent::get_setting($block, 'easing-animation', 'elastic');

		$scroll_speed = parent::get_setting($block,'scroll_speed', '800');
		//	$transition_animation = parent::get_setting($block,'transition-animation', 'left');
		$show_pager = parent::get_setting($block, 'show-pager', '.pager');

		$js = '';

		/**
		 * Typer JS
		 */
		
		if(parent::get_setting($block, 'typer-js')){
		
		$js .= "
		jQuery(document).ready(function($) {
  			$('[data-typer-targets]').typer(
      			{
      			tapeColor			: '#000',
				highlightSpeed    : 20,
				typeSpeed         : 100,
				clearDelay        : 500,
				typeDelay         : 100,
				clearOnHighlight  : true,
				//typerDataAttr     : 'data-typer-targets',
				typerInterval     : 1500
				}
			);
   		});
		";
		}

		/**
		 * Slider
		 */

		$js .= "
		!function ($) {
		$(window).load(function(){
	
		$('.flipper').each(function(){
			
	    	var flipper = $(this)
			,	scrollSpeed
			, 	easing
			, 	direction
			,	responsive
			, 	shownmin
			, 	shownmax
			, 	shownmin = flipper.data('shown-min') || 1
			, 	shownmax = flipper.data('shown-max') || 1
			,	scrollSpeed = flipper.data('scroll-speed') || 800
			,	easing = flipper.data('easing') || 'linear'
			,	fx = flipper.data('fx') || 'fade'
			,	direction = flipper.data('direction') || left
			,	responsive = flipper.data('responsive') || false
			,	width = flipper.data('width') || '100%'
		
	    	flipper.carouFredSel({

						// height: '100%'
			 			height: 'variable'
				 		,direction: direction    // The direction to scroll the carousel. Possible values: 'right', 'left', 'up' or 'down'.
	    		 		, responsive: responsive
	    				
	    				//, responsive: true
	    				//,circular: 	true

				, onCreate: function(){
					
				}

			//	,pagination: '.pager'
				,pagination: '".$show_pager."'

				, items       : {
					width				: width,
					// width			: 250,
					// width			: '100%',
					//	height			: '100%',

				height				: 'variable',
			        visible     : {
			            min         : shownmin
			            , max         : shownmax
			        }
			    }
				
			    , swipe       : {
			        	onTouch     : true
			    }
			    , scroll: {
				    	items: '+1',
				    	easing          : easing
			            ,duration        : scrollSpeed
				        ,fx              : fx
						,pauseOnHover    : true
			    }
				
				,auto     : {
				    play            : true
		        	,duration        :  scroll.duration
					,timeoutDuration :  2 * scroll.duration
				// ,timeoutDuration :  15000 //5 * auto.duration
				}
				
		        , prev    : {
			        button  : function() {
			           return flipper.parents('.flipper-wrap').prev('.flipper-heading').find('.flipper-prev');
			        }
		    	}
			    , next    : {
		       		button  : function() {
			           return flipper.parents('.flipper-wrap').prev('.flipper-heading').find('.flipper-next');
			        },

				scroll: {
						onBefore: function( data ) {
							$('#wrapper-w6k54fb02589759d').animate({
								width: data.width,
								height: data.height,
								marginTop: -(data.height / 2),
								marginLeft: -(data.width / 2) - 100
							}, {
								duration: data.scroll.duration
							});
						}
					}

			    }

		    }).addClass('flipper-loaded').animate({'opacity': 1},800);
		
			//	$.plCommon.plVerticalCenter('.flipper-info', '.pl-center', -20)
		
	    	});
		
		})
		}(window.jQuery);
		";

		return $js;
	}

	function dynamic_css($block_id) {

	//	
	$caption_margin_val = parent::get_setting($block_id, 'caption-margin-val', '70');
	$caption_margin_units = parent::get_setting($block, 'caption-margin-units', '%');

	$caption_margin = $caption_margin_val.$caption_margin_units;
	
	$caption_after_over = parent::get_setting($block_id, 'caption-after-over');

	if ($caption_after_over == 'caption-after' || $caption_after_over == 'caption-over') {

		return '

		#block-'.$block_id.' .flipper-item .item-content {
			top: '.$caption_margin.';
		}';

	}

	}


	/**
	 * Prida class pre ikonku - generuje sa z ACF pola -> "slider_icon_class"
	 * @return [string] [clas="some_icon_class"]
	 */
	function slider_icon_class (){
		if (function_exists('get_field') && get_field('slider_icon_class')){
			echo 'class="'. get_field('slider_icon_class').'"';
		}
	}

/**

Excerpt / Content

**/
function excerpt_content($block, $excerpt_content_class){

/**
  Item Title
 */
  		$html_tag = parent::get_setting($block, 'title-html-tag', 'h3');
		$show_item_title = parent::get_setting($block, 'show-item-title', false);
		$show_item_title_before = parent::get_setting($block, 'show-item-title-before', true);
		//$linked = parent::get_setting($block,'item-title-link', true);
		// $show_titles = parent::get_setting($block, 'show-titles', true);

		$show_continue = parent::get_setting($block, 'show-continue', false);
		$content_to_show = parent::get_setting($block, 'content-to-show', 'excerpt');
/** 
Show Excerpt
**/
		$show_item_excerpt_content = parent::get_setting($block, 'show-item-excerpt-content');
		$item_excerpt_content = parent::get_setting($block, 'item-excerpt-content');	
/**
Advanced Excerpt
**/
		$my_excerpt_limit = parent::get_setting($block, 'my-excerpt-limit');
		$length_type = parent::get_setting($block, 'length-type'); // words, characters
		$no_custom = parent::get_setting($block, 'no-custom'); // 1, 0  (1 = excerpt  /  0 = custom excerpt will be used)
		$no_shortcode = parent::get_setting($block, 'no-shortcode'); // 1, 0  (1 = shortcode remove)  
		$finish = parent::get_setting($block, 'finish'); //exact, word, sentence
		$read_more = parent::get_setting($block, 'read-more'); //
		$add_link = parent::get_setting($block, 'add-link'); //
		$exclude_tags = parent::get_setting($block, 'exclude-tags'); //
		$allowed_tags = parent::get_setting($block, 'allowed-tags'); //
		$ellipsis = parent::get_setting($block, 'ellipsis'); //
/**
END Advanced Excerpt
**/

if ( $show_item_excerpt_content == 'show-option') { ?>
<div class="<?php echo $excerpt_content_class; ?>">
<?php 
	if (($show_item_title) and ($show_item_title_before)) {
		if ($title_length > $limit) 
			$title .= "...";
		if (!$shorten)
			 $title = get_the_title($id);
		if(!$item_linked)
			$item_title = '<' . $html_tag . ' class="item-title">'. $title.'</' . $html_tag . '>';
			if($item_linked)
			$item_title = '<' . $html_tag . ' class="item-title">
			<a href="'. get_post_permalink($id) .'" rel="bookmark" title="'. the_title_attribute (array('echo' => 0) ) .'">'. $title .'</a>
			</' . $html_tag . '>';
		echo $item_title;
	}
?>
	<div class="item-excerpt">
	<?php
		if($item_excerpt_content ==	'excerpt-option'){
			the_excerpt(); 
		} elseif ($item_excerpt_content ==	'content-option'){
			the_content( );
		} elseif ($item_excerpt_content ==	'my-excerpt-option'){

			if(function_exists(the_advanced_excerpt)){

				$params = array( 
					'length' => $my_excerpt_limit,
					'length_type' => $length_type,
					'finish' => $finish,
					'no_custom' => $no_custom,
					'ellipsis'	=> $ellipsis,
					'exclude_tags'	=> $exclude_tags,
					'allowed_tags'	=> $allowed_tags,
					'read_more'	=> $read_more,
					'add_link'	=> $add_link,
					);

				the_advanced_excerpt($params); 
			}
		}
	?>
	</div><!-- excerpt -->
</div> 
<?php  } 

}

/**

Featured Image

**/
function featured_image($block, $featured_image_class){

	$show_images = parent::get_setting($block, 'show-images', true);

	if ($show_images) {

	if ( has_post_thumbnail() ) {
		echo get_the_post_thumbnail( $post->ID, 'featured-thumb', array('title' => ''));
	}
	 $featured_image = wp_get_attachment_image_src( get_post_thumbnail_id(), 'featured' );
	}  // end if images 
}

/**

Content - Anything in here will be displayed when the block is being displayed.

**/
function get_image_sizes( $size = '' ) {

        global $_wp_additional_image_sizes;

        $sizes = array();
        $get_intermediate_image_sizes = get_intermediate_image_sizes();

        // Create the full array with sizes and crop info
        foreach( $get_intermediate_image_sizes as $_size ) {

                if ( in_array( $_size, array( 'thumbnail', 'medium', 'large' ) ) ) {

                        $sizes[ $_size ]['width'] = get_option( $_size . '_size_w' );
                        $sizes[ $_size ]['height'] = get_option( $_size . '_size_h' );
                        $sizes[ $_size ]['crop'] = (bool) get_option( $_size . '_crop' );

                } elseif ( isset( $_wp_additional_image_sizes[ $_size ] ) ) {

                        $sizes[ $_size ] = array( 
                                'width' => $_wp_additional_image_sizes[ $_size ]['width'],
                                'height' => $_wp_additional_image_sizes[ $_size ]['height'],
                                'crop' =>  $_wp_additional_image_sizes[ $_size ]['crop']
                        );

                }

        }

        // Get only 1 size if found
        if ( $size ) {

                if( isset( $sizes[ $size ] ) ) {
                        return $sizes[ $size ];
                } else {
                        return false;
                }

        }

        return $sizes;
}

	function content($block) {


	//var_dump($this->get_image_sizes());

	// foreach ($this->get_image_sizes() as $mage_size => $value) {
		
	// 	echo $mage_size;

	// 	echo $value['width'];
	// 	echo $value['height'];
	// 	echo $value['crop'];
	// }


		$pagination = parent::get_setting($block, 'pagination', FALSE);
		$width = parent::get_setting($block, 'image-width', '260');
		if 	($direction  == 'up' || $direction == 'down'){
			$responsive = 'false';
			$width = '100%';
		} else {
			$responsive = 'true';
			//$width = '240';
		}

		$post_type = parent::get_setting($block, 'post-type', false);
		
		$shown_min = parent::get_setting($block, 'shown-min', 1);		
		$shown_max = parent::get_setting($block, 'shown-max', 4);

		$easing_animation = parent::get_setting($block, 'easing-animation', 'linear');
		$fx = parent::get_setting($block,'fx', 'fade');
		$scroll_speed = parent::get_setting($block,'scroll-speed', '800');

		$title = parent::get_setting($block, 'title');
		$html_tag = parent::get_setting($block, 'title-html-tag', 'h3');

		$hide_link_to_post = parent::get_setting($block,'hide-link-to-post', false);

/**
//	$approx_featured_width = (HeadwayBlocksData::get_block_width($block) / $columns);
pozor hadze tuto chybu Warning: Division by zero in
**/
		$direction = parent::get_setting($block,'direction', 'left');
		
		$show_navigation = parent::get_setting($block, 'show-navigation');
			$prev_nav = parent::get_setting($block, 'prev-nav', 'ss-navigateleft');
			$next_nav = parent::get_setting($block, 'next-nav', 'ss-navigateright');
/**

 Setup Query

**/
			$query_args = array();

			/* Pagination */
				$paged_var = get_query_var('paged') ? get_query_var('paged') : get_query_var('page');

				if ( (parent::get_setting($block, 'paginate', true) || parent::get_setting($block, 'infinite-scroll', true)) && (headway_get('featured-posts-page') || $paged_var) )
					$query_args['paged'] = headway_get('featured-posts-page') ? headway_get('featured-posts-page') : $paged_var;

			/* Categories */
				if ( parent::get_setting($block, 'categories-mode', 'include') == 'include' ) 
					$query_args['category__in'] = parent::get_setting($block, 'categories', array());

				if ( parent::get_setting($block, 'categories-mode', 'include') == 'exclude' ) 
					$query_args['category__not_in'] = parent::get_setting($block, 'categories', array());	
			$query_args['post_type'] = parent::get_setting($block, 'post-type', false);

			/* FeaturedPosts limit */
				$query_args['posts_per_page'] = parent::get_setting($block, 'shown-total', 10);

			/* Author Filter */
				if ( is_array(parent::get_setting($block, 'author')) )
					$query_args['author'] = trim(implode(',', parent::get_setting($block, 'author')), ', ');

			/* Order */
				$query_args['orderby'] = parent::get_setting($block, 'order-by', 'date');
				$query_args['order'] = parent::get_setting($block, 'order', 'DESC');

			/* Status */
				$query_args['post_status'] = 'publish';

			/* Query! */
				$posts = new WP_Query($query_args);

				global $paged; /* Set paged to the proper number because WordPress pagination SUCKS!  ANGER! */
				$paged = $paged_var;
		/* End Query Setup */

		if(!empty($posts)) { the_post( $post ); ?>
			<?php if($title || $show_navigation == 'show-options'){ ?>
			<div class="flipper-heading">
				<div class="flipper-title">
					<?php
					if($linked){
						printf('<%s><a href="%s">%s</a></%s>', $html_tag, self::get_archive_link($post_type), $title, $html_tag );
					} else {
						printf('<%s>%s</%s>', $html_tag, $title, $html_tag );
					}
					?>
				</div>
				<?php if ($show_navigation == 'show-options') {  ?>
				<a class="flipper-prev" href="#"><i class="<?php echo $prev_nav; ?>"></i></a>
		    	<a class="flipper-next" href="#"><i class="<?php echo $next_nav; ?>"></i></a>
		    	<?php  } //end if navigation ?>
			</div>
			<?php } ?>
			<div class="flipper-wrap">
		<ul class="flipper-items text-align-center flipper" data-responsive="<?php echo $responsive; ?>" data-height="variable"  data-width="<?php echo $width; ?>" data-scroll-speed="<?php echo $scroll_speed; ?>" data-easing="<?php echo $easing_animation; ?>" data-direction="<?php echo $direction; ?>" data-fx="<?php echo $fx; ?>" data-shown-min="<?php echo $shown_min; ?>" data-shown-max="<?php echo $shown_max; ?>"> 
		<?php
		$info_over = parent::get_setting($block, 'info-over', FALSE);
		$info_over_text = parent::get_setting($block, 'info-over-text', 'Zobraziť');

		$caption_after_over = parent::get_setting($block, 'caption-after-over', 'caption-over');
		?>
		<?php } 
			if(!empty($posts)):
				while ( $posts->have_posts() ) : $posts->the_post();
				?>
			<li <?php $this->slider_icon_class(); ?>>
			<div class="flipper-item fix">
			<?php $this->featured_image($block, $featured_image_class); ?>
				<?php if ($caption_after_over =='caption-over'): ?>
				<div class="item-content" style="max-width: 1000px;">
					<div class="item-content-pad">
					<?php $this->excerpt_content($block, 'caption-over'); ?>
					</div>
				</div>
			<?php endif; ?>
			<!-- Info -->
			<?php if ($info_over): ?>
			<div class="flipper-info-bg"></div>
			<?php if(function_exists(get_field) && get_field('link_to_another_post') ){
			 foreach(get_field('link_to_another_post') as $post_object): ?>
			 <a class="flipper-info pl-center-inside" href="<?php echo get_permalink($post_object->ID); ?>">
				<div class="pl-center">
					<?php printf('<div class="info-text">%s</div>', $info_over_text); ?>
				</div>
    		</a>
			<?php endforeach; 
			} else { ?>
			<a class="flipper-info pl-center-inside" href="<?php echo get_permalink(); ?>">
				<div class="pl-center">
					<?php printf('<div class="info-text">%s</div>', $info_over_text); ?>
				</div>
			</a>
		<?php	} ?>
			<?php endif; ?>
			</div> <!-- End flipper -->
			<?php if ($caption_after_over == 'caption-after'): ?>
				<?php $this->excerpt_content($block, 'caption-after'); ?>
			<?php endif ?>
			</li>
			<?php
		endwhile;
		endif;
			if(!empty($posts))
		 		echo '</ul>';
		 	if ($pagination) {
		 		echo '<div class="pager"></div>';
		 	}
			echo '<div class="clear"></div></div>';
/** 
	
	DEBUG 

**/		
	if (parent::get_setting($block, 'debug') == true) 	{
		$this->debug_mode($block);
	}
}

function debug_mode($block) {

	echo '<div class="debug">
	 <pre>';
		print_r($query_args);
		echo 'width - '. $width .'<br>';

		echo 'easing - '. $easing_animation .'<br>';
		echo 'responsive - '.  $responsive.'<br>';
		echo 'fx - '.  $fx .'<br>';
		echo 'scroll speed - '. $scroll_speed .'<br>';
		echo 'direction - '. $direction .'<br>';
		echo 'shown_max - '. $shown_max .'<br>';
		echo 'shown_min - '. $shown_min .'<br>';

		echo 'Prev Nav - '. $prev_nav .'<br>';
		echo 'Next Nav - '. $next_nav .'<br>';
		echo 'Content - '. $content_to_show .'<br>';
		echo 'Show navigation - '. $show_navigation .'<br>';
		echo 'Show Item Title - '. $show_item_title .'<br>';
		echo 'Show Item Excerpt mode - '. $item_excerpt_content .'<br>';

		echo 'Show Item Title Before - '. $show_item_title_before .'<br>';
		echo '</pre></div>';
}

	/**
	 * Register elements to be edited by the Headway Design Editor
	 **/

	function setup_elements() {

		//	'properties' => array('fonts', 'background', 'borders', 'padding', 'rounded-corners', 'box-shadow', 'text-shadow'),
		// 'parent' => 'flipper-prev-next',

		$this->register_block_element(array(
			'id' => 'featured-title',
			'name' => 'Featured Posts Title',
			'selector' => '.featured-posts-featured .entry-title a',
			'properties' => array('fonts', 'background', 'borders', 'padding', 'rounded-corners', 'box-shadow', 'text-shadow'),
			'states' => array(
				'Hover' => '.featured-posts-featured .entry-title a:hover', 
			)
		));

		$this->register_block_element(array(
			'id' => 'flipper-meta',
			'name' => 'Meta',
			'selector' => '.flipper-meta',
			'states' => array(
				'Hover' => '.flipper-meta:hover', 
			)
		));
		
		$this->register_block_element(array(
			'id' => 'flipper-post-title',
			'name' => 'Title',
			'selector' => 'h4.flipper-post-title',
			'states' => array(
				'Hover' => 'h4 .flipper-post-title:hover', 
			)
		));
	
		$this->register_block_element(array(
			'id' => 'flipper-item',
			'name' => 'Item',
			'selector' => '.flipper-item,',
			'states' => array(
				'Hover' => '.flipper-item:hover', 
			)
		));

		$this->register_block_element(array(
			'id' => 'flipper-item-image',
			'name' => 'Item Image',
			'selector' => '.flipper-item img',
			'states' => array(
				'Hover' => '.flipper-item img:hover', 
			)
		));

/**
 * Caption 
 */
		$this->register_block_element(array(
			'id' => 'item-content',
			'name' => 'Caption over',
			//'parent' => '', // parent id
			'selector' => '.flipper-item .item-content',
		));

/**
 * Caption Over
 */
			$this->register_block_element(array(
				'id' => 'caption-over-h',
				'name' => 'H',
				'parent' => 'item-content', // parent id
				'selector' => '.caption-over h2, .caption-over h3, .caption-over h4, .caption-over h5, .caption-over h6, .caption-over span',
			));

			$this->register_block_element(array(
				'id' => 'caption-over-p',
				'name' => 'p',
				'parent' =>'item-content', // parent id
				'selector' => '.caption-over p',
			));

/**
 * Caption After
 */
		$this->register_block_element(array(
			'id' => 'caption-after',
			'name' => 'Caption After',
			//'parent' => '', // parent id
			'selector' => '.caption-after',
		));

			$this->register_block_element(array(
				'id' => 'caption-after-h',
				'name' => 'H',
				'parent' => 'caption-after', // parent id
				'selector' => '.caption-after h2, .caption-after h3, .caption-after h4, .caption-after h5, .caption-after h6, .caption-after span',
			));

			$this->register_block_element(array(
				'id' => 'caption-after-p',
				'name' => 'p',
				'parent' =>'caption-after', // parent id
				'selector' => '.caption-after p',
			));

			$this->register_block_element(array(
				'id' => 'caption-after-icon',
				'name' => 'Icon',
				'parent' => 'caption-after', // parent id
				'selector' => '.caption-after:before',
				'inspectable' => true,
				// 'description' => '',
			));

		$this->register_block_element(array(
			'id' => 'item-content-pad',
			'name' => 'Content pad',
			'parent' => 'item-content', // parent id
			'selector' => '.item-content-pad',
		));

		$this->register_block_element(array(
			'id' => 'flipper-item-content-h2',
			'name' => 'Head',
			'parent' => 'item-content', // parent id
			'selector' => '.flipper-item .item-content h2 a',
			'description' => 'description',
		));

		$this->register_block_element(array(
			'id' => 'flipper-heading-title',
			'name' => 'Heading Title',
			'selector' => '.flipper-heading .flipper-title',
			'states' => array(
				'Hover' => '.flipper-heading .flipper-title:hover', 
			)
		));
		
		$this->register_block_element(array(
			'id' => 'flipper-heading-title-h',
			'name' => 'Heading Title H',
			'selector' => '.flipper-title h1, .flipper-title h2, .flipper-title h3, .flipper-title h4, .flipper-title h5, .flipper-title h6, .flipper-title span',
			'states' => array(
				'Hover' => '.flipper-title h1:hover, .flipper-title h2:hover, .flipper-title h3:hover, .flipper-title h4:hover, .flipper-title h5:hover, .flipper-title h6:hover, .flipper-title span:hover', 
			)
		));

		$this->register_block_element(array(
			'id' => 'read-more',
			'name' => 'Read more',
			'selector' => 'div.flipper-meta > a.read-more',
			'states' => array(
				'Hover' => ' div.flipper-meta > a.read-more:hover', 
			)
		));
			// 'parent' => 'flipper-prev-next',

		$this->register_block_element(array(
			'id' => 'pager',
			'name' => 'Pager',
			'selector' => '.flipper-wrap > div.pager a',
			'states' => array(
				'Hover' => '.flipper-wrap > div.pager a:hover', 
			)
		));

/**
 * Navigation
 */
		$this->register_block_element(array(
			'id' => 'flipper-prev-next',
			'name' => 'Prev / Next',
			'selector' => '.flipper-next, .flipper-prev',
			'states' => array(
				'Hover' => '.flipper-next:hover, .flipper-prev:hover', 
			)
		));

		$this->register_block_element(array(
			'id' => 'flipper-prev',
			'name' => 'Prev',
			'parent' => 'flipper-prev-next',
			'selector' => '.flipper-prev',
			'states' => array(
				'Hover' => '.flipper-prev:hover', 
			)
		));

		$this->register_block_element(array(
			'id' => 'flipper-next',
			'name' => 'Next',
			'selector' => '.flipper-next',
			'parent' => 'flipper-prev-next',
			'states' => array(
				'Hover' => '.flipper-next:hover', 
			)
		));
/**
 * Pager
 */
		$this->register_block_element(array(
			'id' => 'pager-selected',
			'name' => 'Pager Selected',
			'parent' => 'pager',
			'selector' => '.flipper-wrap > div.pager a.selected',
			'states' => array(
				'Hover' => '.flipper-wrap > div.pager a.selected:hover', 
			)
		));
	}
	
}