<?php
class HeadwayFeaturedPostsBlockOptions extends HeadwayBlockOptionsAPI {
	
	public $tabs = array(
 		'query-filters' => 'Query Filters',
		'featured-setup' => 'Features Setup',
		'captions' => 'Caption',
		'special' => 'Special'

	);

	public $inputs = array(

		'query-filters' => array(

         'opt-name'	=> array(
         	'name'	 => 'image-size',
         	'type'	 => 'select',
         	'label'	 => 'Image Size',
         	'options' => 'get_image_size()'
         ),
			'post-type' => array(
				'type' => 'select',
				'name' => 'post-type',
				'label' => 'Post Type',
				// 'tooltip' => '',
				'options' => 'get_post_types()'
			),

			'categories' => array(
				'type' => 'multi-select',
				'name' => 'categories',
				'label' => 'Categories',
				'options' => 'get_categories()',
				'tooltip' => 'Filter the posts that are shown by categories.'
			),
			
			'categories-mode' => array(
				'type' => 'select',
				'name' => 'categories-mode',
				'label' => 'Categories Mode',
				// 'tooltip' => '',
				'options' => array(
					'include' => 'Include',
					'exclude' => 'Exclude'
				),
				'tooltip' => 'If this is set to <em>include</em>, then only the pins that match the categories filter will be shown.  If set to <em>exclude</em>, all pins that match the selected categories will not be shown. '
			),
			
			'order' => array(
				'type' => 'select',
				'name' => 'order',
				'label' => 'Order',
				// 'tooltip' => '',
				'options' => array(
					'desc' => 'Descending',
					'asc' => 'Ascending',
				)
			)
		),

	'featured-setup' => array(
			'shown-total' => array(
				'type' => 'slider',
				'name' => 'shown-total',
				'label' => 'Total',
				'slider-min' => 3,
				'slider-max' => 24,
				'slider-interval' => 1,
				'default' => 10,
				// 'tooltip' => 'How many posts to show.',
				'callback' => ''
			),

			'shown-min' => array(
				'type' => 'slider',
				'name' => 'shown-min', 
				'label' => 'Shown Min',
				'slider-min' => 1,
				'slider-max' => 12,
				'slider-interval' => 1,
				'default' => 1,
				// 'tooltip' => 'Show min post'
			),

			'shown-max' => array(
				'type' => 'slider',
				'name' => 'shown-max', 
				'label' => 'Shown max',
				'slider-min' => 1,
				'slider-max' => 12,
				'slider-interval' => 1,
				'default' => 4,
				// 'tooltip' => 'Show max post'
			),

		// Heading
		'navigation-heading'	=> array(
			'name'	 => 'navigation-heading',
			'type'	 => 'heading',
			'label'	 => 'Navigation'
		),
			'show-navigation'	=> array(
				'name'	 => 'show-navigation',
				'type'	 => 'select',
				'label'	 => 'Navigation',
			//	'tooltip' => '',
				'options' => array(
					'hide-options' => 'Hide',
					'show-options' => 'Show'
				),
				'toggle' => array(
					'hide-options' => array(
						'hide' => array(
							'#input-prev-nav',
							'#input-next-nav'
						),
					),
					'show-options' => array(
						'show' => array(
							'#input-prev-nav',
							'#input-next-nav'
						),
					),
				),
				'callback' => 'id = $(input).attr("block_id");'
			),

			'prev-nav'	=> array(
				'name'	 => 'prev-nav',
				'type'	 => 'select',
				'label'	 => 'Navigation Prev',
				'default' => 'ss-navigateleft',
				'options' => array(
					'ss-navigateleft' => 'ss-navigateleft',
					'fa fa-caret-left' => 'fa fa-caret-left',
			 		//'right' => 'right'
				),
			//	'tooltip' => ''
			),

			'next-nav'	=> array(
				'name'	 => 'next-nav',
				'type'	 => 'select',
				'label'	 => 'Navigation Next',
				'default' => 'ss-navigateright',
				'options' => array(
					'ss-navigateright' => 'ss-navigateright',
					'fa fa-caret-right' => 'fa fa-caret-right',			 		
					//'right' => 'right'
				),
			//	'tooltip' => ''
			),

			'pagination'	=> array(
				'name'	 => 'pagination',
				'type'	 => 'checkbox',
				'label'	 => 'Pagination',
				'default' => false,
				// 'tooltip' => 'tooltip'
			),
			

		/**

		Image

		**/

		//Heading
		'heading-image'	=> array(
			'name'	 => 'heading-image',
			'type'	 => 'heading',
			'label'	 => 'Image'
		),
			'show-images' => array(
				'type' => 'checkbox',
				'name' => 'show-images', 
				'label' => 'Show Images',
				'default' => true,
			),

			'hide-link-to-post'	=> array(
				'name'	 => 'hide-link-to-post',
				'type'	 => 'checkbox',
				'label'	 => 'Hide link to post',
				'default' => false,
				// 'tooltip' => ''
			),

			'image-width'	=> array(
				'name'	 => 'image-width',
				'type'	 => 'text',
				'label'	 => 'Image width',
				//'tooltip' => ''
			),
/**
Animation
**/

	// Heading
		'animation-heading'	=> array(
			'name'	 => 'animation-heading',
			'type'	 => 'heading',
			'label'	 => 'Animation'
		),

		'direction'	=> array(
			'name'	 => 'direction',
			'type'	 => 'select',
			'label'	 => 'Direction',
			'default' => 'left',
			'options' => array(
				'left' => 'left',
		 		'right' => 'right',
		 		'up' => 'up',
		 		'down' => 'down'
			),
		//'tooltip' => ''
		),

		'fx'	=> array(
			'name'	 => 'fx',
			'type'	 => 'select',
			'label'	 => 'FX',
			'default' => 'left',
			'options' => array(
				'fade' => 'fade',
				'scroll' => 'scroll',
				'directscroll' => 'directscroll',
		 		'crossfade' => 'crossfade',
		 		'cover' => 'cover',
		 		'cover-fade' => 'cover-fade',
		 		'uncover' => 'uncover',
		 		'uncover-fade' => 'uncover-fade',
		 		'up' => 'up',
		 		'down' => 'down'
			),
		//'tooltip' => ''
		),
			'easing-animation'	=> array(
				'name'	 => 'easing-animation',
				'type'	 => 'select',
				'label'	 => 'Easing animation',
				'default' => 'linear',
				'options' => array(
					"linear" => "linear" ,
					"swing" => "swing",
					"easeInQuad" => "easeInQuad" ,
					"easeOutQuad" => "easeOutQuad",
					"easeInOutQuad" => "easeInOutQuad",
					"easeInCubic" => "easeInCubic" ,
					"easeOutCubic" => "easeOutCubic" ,
					"easeInOutCubic" => "easeInOutCubic" ,
					"easeInQuart" => "easeInQuart" ,
					"easeOutQuart" => "easeOutQuart" ,
					"easeInOutQuart" => "easeInOutQuart",
					"easeInQuint" => "easeInQuint",
					"easeOutQuint" => "easeOutQuint",
					"easeInOutQuint" => "easeInOutQuint",
					"easeInExpo" => "easeInExpo" ,
					"easeOutExpo" => "easeOutExpo",
					"easeInOutExpo" => "easeInOutExpo" ,
					"easeInSine" => "easeInSine" ,
					"easeOutSine" => "easeOutSine" ,
					"easeInOutSine" => "easeInOutSine",
					"easeInCirc" => "easeInCirc" ,
					"easeOutCirc" => "easeOutCirc" ,
					"easeInOutCirc" => "easeInOutCirc" ,
					"easeInElastic" => "easeInElastic" ,
					"easeOutElastic" => "easeOutElastic" ,
					"easeInOutElastic" => "easeInOutElastic",
					"easeInBack" => "easeInBack" ,
					"easeOutBack" => "easeOutBack" ,
					"easeInOutBack" => "easeInOutBack",
					"easeInBounce" => "easeInBounce" ,
					"easeOutBounce" => "easeOutBounce" ,
					"easeInOutBounce" => "easeInOutBounce" 
				),
				// 'tooltip' => ''
			),

			'scroll-speed'	=> array(
				'name'	 => 'scroll-speed',
				'type'	 => 'slider',
				'slider-min' => 600,
				'slider-max' => 10000,
				'slider-interval' =>100,
				'label'	 => 'Scroll Speed',
				'default' => 3800,
				//'tooltip' => ''
			),

		// Heading
		'heading-title'	=> array(
			'name'	 => 'heading-title',
			'type'	 => 'heading',
			'label'	 => 'Title Before Carousel'
		),
			'title'	=> array(
				'name'	 => 'title',
				'type'	 => 'text',
				'label'	 => 'Title',
				//'tooltip' => ''
			),

			'title-html-tag' => array(
				'type' => 'select',
				'name' => 'title-html-tag',
				'label' => 'Title HTML tag',
				'default' => 'h1',
				'options' => array(
					'h1' => '&lt;H1&gt;',
					'h2' => '&lt;H2&gt;',
					'h3' => '&lt;H3&gt;',
					'h4' => '&lt;H4&gt;',
					'h5' => '&lt;H5&gt;',
					'h6' => '&lt;H6&gt;',
					'span' => '&lt;span&gt;'
				)
			),

			'title-link' => array(
				'type' => 'checkbox',
				'name' => 'title-link',
				'label' => 'Link Title?'
			),
			
		// Heading
		'debug-heading'	=> array(
			'name'	 => 'debug-heading',
			'type'	 => 'heading',
			'label'	 => 'Debug Info'
		),
			
			'debug' => array(
				'type' => 'checkbox',
				'name' => 'debug', 
				'label' => 'Show: DEBUG',
				'default' => false,
			),
		),

	/**
EXCERPT
**/
	'captions' => array(

	'heading-title-content'	=> array(
		'name'	 => 'heading-title-content',
		'type'	 => 'heading',
		'label'	 => 'Item title / Edit link'
	),
			
		'show-item-title'	=> array(
			'name'	 => 'show-item-title',
			'type'	 => 'checkbox',
			'label'	 => 'Show Item Title',
			'default' => false,
			// 'tooltip' => 'tooltip'
		),

		'show-item-title-before'	=> array(
			'name'	 => 'show-item-title-before',
			'type'	 => 'checkbox',
			'label'	 => 'Title before content',
			'default' => true,
		//	'tooltip' => 'tooltip'
		),

		'show-edit-link'	=> array(
			'name'	 => 'show-edit-link',
			'type'	 => 'checkbox',
			'label'	 => 'Show Edit',
			'default' => false,
			// 'tooltip' => 'tooltip'
		),

		'info-over'	=> array(
			'name'	 => 'info-over',
			'type'	 => 'checkbox',
			'label'	 => 'Info Over',
			'default' => false,
			// 'tooltip' => 'tooltip'
		),
		
		'info-over-text'	=> array(
			'name'	 => 'info-over-text',
			'type'	 => 'text',
			'label'	 => 'Info Over Text',
			'default' => 'Zobraziť'
			// 'tooltip' => 'tooltip'
		),

		'heading-item-excerpt-content'	=> array(
			'name'	 => 'headind-item-excerpt-content',
			'type'	 => 'heading',
			'label'	 => 'Excerpt / Content'
		),

		'caption-after-over' => array(
			'name'	 => 'caption-after-over',
			'type'	 => 'select',
			'label'	 => 'Caption After Over',
			'default' => 'caption-after',
			'options' => array(
			    '' => 'None',
				'caption-after' => 'Caption After',
		 		'caption-over' => 'Caption Over'
			),
			// 'tooltip' => ''
		),

		'caption-margin-val' => array(
			'name'	 => 'caption-margin-val',
			'type'	 => 'text',
			'default' => '80',
			'label'	 => 'Caption Margin',
			// 'tooltip' => 'tooltip'
		),
		'caption-margin-units'	=> array(
			'name'	 => 'caption-margin-units',
			'type'	 => 'select',
			'label'	 => 'Caption Margin Units',
			'default' => '%',
			'options' => array(
				'%' => '%',
		 		'px' => 'px',
		 		'em' => 'em'
			),
			// 'tooltip' => ''
		),

		'show-item-excerpt-content'	=> array(
			'name'	 => 'show-item-excerpt-content',
			'type'	 => 'select',
			'label'	 => 'Show Excerpt',
			// 'tooltip' => 'tooltip',
			'options' => array(
				'hide-option' => 'Hide',
				'show-option' => 'Show'
			),
			'toggle' => array(
				'hide-option' => array(
					'hide' => array(
						'#input-show-item-excerpt',
						'#input-item-excerpt-content'
					),
				),
				'show-option' => array(
					'show' => array(
						'#input-show-item-excerpt',
						'#input-item-excerpt-content'
					),
				),
			),
			'callback' => 'id = $(input).attr("block_id");'
		),

		'item-excerpt-content'	=> array(
			'name'	 => 'item-excerpt-content',
			'type'	 => 'select',
			'label'	 => 'Customize mode',
			// 'tooltip' => '',
			'options' => array(
				'excerpt-option' => 'Excerpt',
				'my-excerpt-option' => 'Advanced Excerpt',
				'content-option' => 'Content'
			),
			'toggle' => array(
				'excerpt-option' => array(
					'hide' => array(
						'#input-my-excerpt-limit',
						'#input-length-type',
						'#input-finish',
						'#input-no-custom',
						'#input-add-link',
						'#input-read-more',
						'#input-ellipsis',
						'#input-no-shortcode',
						'#input-heading-advanced-excerpt',
						'#input-exclude-tags',
						'#input-allowed-tags',
					),
				),
				'my-excerpt-option' => array(
					'show' => array(
						'#input-my-excerpt-limit',
						'#input-length-type',
						'#input-finish',
						'#input-no-custom',
						'#input-add-link',
						'#input-read-more',
						'#input-ellipsis',
						'#input-no-shortcode',
						'#input-heading-advanced-excerpt',
						'#input-exclude-tags',
						'#input-allowed-tags',
					),
				),
				'content-option' => array(
					'hide' => array(
						'#input-my-excerpt-limit',
						'#input-length-type',
						'#input-finish',
						'#input-no-custom',
						'#input-add-link',
						'#input-read-more',
						'#input-ellipsis',
						'#input-no-shortcode',
						'#input-heading-advanced-excerpt',
						'#input-exclude-tags',
						'#input-allowed-tags',
					),
				),
			),
			'callback' => 'id = $(input).attr("block_id");'
		),

			'heading-advanced-excerpt'	=> array(
				'name'	 => 'heading-advanced-excerpt',
				'type'	 => 'heading',
				'label'	 => 'Advanced Excerpt'
			),

			'length-type'	=> array(
				'name'	 => 'length-type',
				'type'	 => 'select',
				'label'	 => 'Length Type',
				'default' => 'left',
				'options' => array(
					'words' => 'words',
			 		'characters' => 'characters'
				),
				// 'tooltip' => 'tooltip'
			),

			'my-excerpt-limit'	=> array(
				'name'	 => 'my-excerpt-limit',
				'type'	 => 'slider',
				'slider-min' => 10,
				'slider-max' => 150,
				'slider-interval' =>1,
				'label'	 => 'My Excerpt Limit',
				'default' => 10,
				// 'tooltip' => ''
			),

			'no-custom'	=> array(
				'name'	 => 'no-custom',
				'type'	 => 'checkbox',
				'label'	 => 'no-custom',
				'default' => false,
				// 'tooltip' => ''
			),

			'add-link'	=> array(
				'name'	 => 'add-link',
				'type'	 => 'checkbox',
				'label'	 => 'add-link',
				'default' => false,
				// 'tooltip' => ''
			),

			'read-more'	=> array(
				'name'	 => 'read-more',
				'type'	 => 'text',
				'label'	 => 'Read more text',
				'tooltip' => 'enumeration, if set to `exact` the excerpt will be the exact lenth as defined by the "Excerpt Length" option. If set to `word` the last word in the excerpt will be completed. If set to `sentence` the last sentence in the excerpt will be completed'
			),

			'finish'	=> array(
				'name'	 => 'finish',
				'type'	 => 'select',
				'label'	 => 'finish',
				'default' => 'left',
				'options' => array(
					// exact, word, sentence
					'exact' => 'exact',
			 		'word' => 'word',
			 		'sentence' => 'sentence'
				),
				// 'tooltip' => ''
			),

			'no-shortcode'	=> array(
				'name'	 => 'no-shortcode',
				'type'	 => 'checkbox',
				'label'	 => 'No Shortcode',
				'default' => false,
				// 'tooltip' => ''
			),

			'ellipsis'	=> array(
				'name'	 => 'ellipsis',
				'type'	 => 'text',
				'label'	 => 'ellipsis',
				'tooltip' => '&amp;hellip;'
			),
			'exclude-tags'	=> array(
				'name'	 => 'exclude-tags',
				'type'	 => 'text',
				'label'	 => 'Exclude Tags',
				'tooltip' => 'img,p,strong  a comma-separated list of HTML tags that must be removed from the excerpt. Using this setting in combination with `allowed_tags` makes no sense'
			),

			'allowed-tags'	=> array(
				'name'	 => 'allowed-tags',
				'type'	 => 'text',
				'label'	 => 'Allowed Tags',
				'tooltip' => 'img,p,strong  a comma-separated list of HTML tags that must be removed from the excerpt. Using this setting in combination with `allowed_tags` makes no sense'
			),
	
	),
	'special' => array(
		    'typer-js'	=> array(
	       	'name'	 => 'typer-js',
	       	'type'	 => 'checkbox',
	       	'label'	 => 'Typer.js',
	       	'default' => false,
	       	// 'tooltip' => 'tooltip'
	       ),
)
	);

/**
 * [get_categories description]
 * @return [type] [description]
 */
	function get_categories() {
		
		$category_options = array();
		
		// $args = array(
		// 	'type' => 'skider',
		// 	'orderby' => $this->get_post_types(),
		// 	'order' => 'ASC'
		// );

		$categories_select_query = get_categories();
		
		foreach ($categories_select_query as $category)
			$category_options[$category->term_id] = $category->name;

		return $category_options;
	}
	


/**
 * [get_image_size description]
 * @return [array] [name / name]
 */

function get_image_sizes( $size = '' ) {

        global $_wp_additional_image_sizes;

        $sizes = array();
        $get_intermediate_image_sizes = get_intermediate_image_sizes();

        // Create the full array with sizes and crop info
        foreach( $get_intermediate_image_sizes as $_size ) {

                if ( in_array( $_size, array( 'thumbnail', 'medium', 'large' ) ) ) {

                        $sizes[ $_size ]['width'] = get_option( $_size . '_size_w' );
                        $sizes[ $_size ]['height'] = get_option( $_size . '_size_h' );
                        $sizes[ $_size ]['crop'] = (bool) get_option( $_size . '_crop' );

                } elseif ( isset( $_wp_additional_image_sizes[ $_size ] ) ) {

                        $sizes[ $_size ] = array( 
                                'width' => $_wp_additional_image_sizes[ $_size ]['width'],
                                'height' => $_wp_additional_image_sizes[ $_size ]['height'],
                                'crop' =>  $_wp_additional_image_sizes[ $_size ]['crop']
                        );

                }

        }

        // Get only 1 size if found
        if ( $size ) {

                if( isset( $sizes[ $size ] ) ) {
                        return $sizes[ $size ];
                } else {
                        return false;
                }

        }

        return $sizes;
}

	function get_image_size() {
		
		$image_size_options = array();
		$image_sizes = $this->get_image_sizes(); 

		foreach ($image_sizes as $size_name => $size_attrs){
			
			if ($size_attrs['crop']){
				$crop = 'crop';
			}
			
			$image_size_options[$size_name] = $size_name .' '. $size_attrs['width'].'&#215;'.$size_attrs['height']. ' '.$crop;
		}
		return $image_size_options;
	}



/**
 * [Get post types description]
 * @return [array] [post_type_id / post type name]
 */
	function get_post_types() {
		
		$post_type_options = array();

		$post_types = get_post_types(false, 'objects'); 
			
		foreach($post_types as $post_type_id => $post_type){
			
			//Make sure the post type is not an excluded post type. 
			if(in_array($post_type_id, array('revision', 'nav_menu_item'))) 
				continue;
			
			$post_type_options[$post_type_id] = $post_type->labels->name;
		
		}
		
		return $post_type_options;
	}
	

/**
 * [get_authors description]
 * @return [type] [description]
 */
	function get_authors() {
		
		$author_options = array();
		
		$authors = get_users(array(
			'orderby' => 'post_count',
			'order' => 'desc',
			'who' => 'authors'
		));
		
		foreach ( $authors as $author )
			$author_options[$author->ID] = $author->display_name;
			
		return $author_options;
		
	}
	
}